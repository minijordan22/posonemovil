
import 'package:flutter/material.dart';
import 'package:pos_one_movil/widgets/widgets.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({
    super.key,
    required this.emailRegExp,
  });

  final RegExp emailRegExp;

  @override
  Widget build(BuildContext context) {
    final Map<String,String> formData = {
      'email': '',
      'name': '',
    };
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          CustomInputField(
            name: 'email', 
            regExp: emailRegExp, 
            formproperty: 'email', 
            formData: formData,
            hintText: 'Correo electrónico',
            labelText: 'Correo electrónico',
            suffixIcon: Icons.email,
            keyboardType: TextInputType.emailAddress,
          ),
          const SizedBox(
            height: 30,
          ),
          CustomInputField(
            name: 'password', 
            regExp: RegExp(r'^.{6,12}$'), 
            formproperty: 'password', 
            formData: formData,
            hintText: 'Contraseña',
            labelText: 'Contraseña',
            suffixIcon: Icons.lock,
            obscureText: true,
            keyboardType: TextInputType.text,
          ),
          const SizedBox(
            height: 30,
          ),
          ShapeButton(
            labelText: 'Ingresar',
            type: 'primary',
            minWidth: double.infinity,
            horizontalPadding: 80,
            verticalPadding: 15,
            onPressed: () {
              Navigator.popAndPushNamed(context, 'home'); 
              //login();
          }),
          const SizedBox(
            height: 25,
          ),
          ShapeButton(
            labelText: '¿Olvidaste tu contraseña?',
            type: 'accent',
            minWidth: double.infinity,
            horizontalPadding: 80,
            verticalPadding: 0,
            onPressed: () {
              //recuperarPassword();
          }),
        ]
      ),
    );
  }
}