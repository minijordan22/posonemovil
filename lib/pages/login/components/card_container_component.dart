import 'package:flutter/material.dart';

class CardContainerComponent extends StatelessWidget {
  final Widget child;
  final bool widhtPadding;
  const CardContainerComponent({super.key, required this.child, this.widhtPadding = true});

  @override
  Widget build(BuildContext context) {
    Widget card = Column(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          decoration: _createCardShape(),
          child: child,
        ),
      ],
    );
    return widhtPadding
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: card,
          )
        : card;
  }

  BoxDecoration _createCardShape() => BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), boxShadow: const [BoxShadow(color: Colors.black12, blurRadius: 15, offset: Offset(0, 5))]);
}
