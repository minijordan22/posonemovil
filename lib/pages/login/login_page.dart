import 'package:flutter/material.dart';
import 'package:pos_one_movil/pages/login/components/background_component.dart';
import 'package:pos_one_movil/pages/login/components/card_container_component.dart';

import 'components/login_form.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final RegExp emailRegExp = RegExp(
    r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$',
    caseSensitive: false,
    multiLine: false,
  );

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Stack(
          children: [
            size.width > 1060 ? const LoginBackground() : const SizedBox(),
            SingleChildScrollView(child: _ContainerForm(emailRegExp: emailRegExp)),
          ],
        ),
      ),
    );
  }
}

class _ContainerForm extends StatelessWidget {
  const _ContainerForm({
    super.key,
    required this.emailRegExp,
  });

  final RegExp emailRegExp;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Row(
      children: [
        SizedBox(
          width: size.width > 1060 ? size.width * 0.5 : 0,
          height: size.height,
        ),
        _LoginFormArea(size: size, emailRegExp: emailRegExp),
      ],
    );
  }
}

class _LoginFormArea extends StatelessWidget {
  const _LoginFormArea({
    super.key,
    required this.size,
    required this.emailRegExp,
  });

  final Size size;
  final RegExp emailRegExp;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size.width > 1060 ? size.width * 0.5 : size.width,
      height: size.height,
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
          Color.fromRGBO(59, 59, 59, 1),
          Color.fromRGBO(0, 0, 0, 1),
        ])),
        child: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              children: [
                SizedBox(
                  height: size.height * .25,
                ),
                _LoginFormContainer(emailRegExp: emailRegExp),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _LoginFormContainer extends StatelessWidget {
  const _LoginFormContainer({
    super.key,
    required this.emailRegExp,
  });

  final RegExp emailRegExp;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 370, maxWidth: 550),
      child: CardContainerComponent(
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Inicio de sesión',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
            ),
            const SizedBox(
              height: 30,
            ),
            LoginForm(emailRegExp: emailRegExp)
          ],
        ),
      ),
    );
  }
}
