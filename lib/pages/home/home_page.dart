import 'package:flutter/material.dart';
import 'package:sidebarx/sidebarx.dart';
import '../../widgets/widgets.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final _controller = SidebarXController(selectedIndex: 0, extended: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: Row(children: [
          SideBar(controller: _controller),
          Expanded(
            child: Column(
              children: [
                Container(
                  child: Column(children: [
                    Text('Prueba de espacios'),
                    Text('Segundo renglon'),
                    Row(
                      children: [
                        Container(
                          color: Colors.red,
                          width: 250,
                          height: 250,
                        ),
                        _ResumenVenta(),
                      ],
                    )
                  ]),
                ),
                const SizedBox(height: 20),
                CategoryList(),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class _ResumenVenta extends StatelessWidget {
  const _ResumenVenta({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
          height: 250,
          child: Column(
            children: [
              const Text('Resumen de la venta: ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              CartList(),
              const Text('Total de la venta: \$1000', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              Expanded(child: Container()),
              ElevatedButton(onPressed: () {}, child: Text('Finalizar venta')),
            ],
          )),
    );
  }
}
