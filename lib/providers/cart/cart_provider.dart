import 'package:pos_one_movil/models/models.dart';

class CartProvider {
  final List<Article> _cartItems = [];

  List<Article> get cartItems => _cartItems;

  void addToCart(Article item) {
    _cartItems.add(item);
  }

  void removeFromCart(Article item) {
    _cartItems.remove(item);
  }
}
