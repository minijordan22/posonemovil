import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pos_one_movil/models/models.dart';

class CartList extends StatelessWidget {
  CartList({super.key});

  final List<Article> cartList = new List<Article>.generate(15, (index) {
    return Article(
      title: 'Articulo de prueba #$index validadno los espacios',
      description: 'Descripcion del articulo $index',
      url: 'https://cdn-media.glamira.com/media/product/newgeneration/view/1/sku/RV3G/diamond/diamond-Brillant_AAA/alloycolour/red_white.jpg',
      urlToImage: '',
      publishedAt: '',
      content: '1',
    );
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 140,
      child: Expanded(
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return _CartItem(item: cartList[index]);
          },
          itemCount: cartList.length,
        ),
      ),
    );
  }
}

class _CartItem extends StatefulWidget {
  const _CartItem({
    super.key,
    required this.item,
  });

  final Article item;

  @override
  State<_CartItem> createState() => _CartItemState();
}

class _CartItemState extends State<_CartItem> {
  late int content;

  @override
  void initState() {
    super.initState();
    content = int.parse(widget.item.content);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage(widget.item.url),
                  ),
                  IconButton(
                    icon: const Icon(Icons.add),
                    onPressed: () {
                      setState(() {
                        content++;
                      });
                    },
                  ),
                  Text('$content'),
                  IconButton(
                    icon: const Icon(Icons.remove),
                    onPressed: () {
                      setState(() {
                        if (content > 1) {
                          content--;
                        }
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                width: 150,
                child: Text(widget.item.title, textAlign: TextAlign.center, style: textTheme.bodySmall, overflow: TextOverflow.ellipsis, maxLines: 2),
              ),
              const Text('\$100')
            ],
          ),
        ),
      ),
    );
  }
}
