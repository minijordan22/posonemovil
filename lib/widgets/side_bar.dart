import 'package:flutter/material.dart';
import 'package:sidebarx/sidebarx.dart';

class SideBar extends StatelessWidget {
  const SideBar({
    super.key,
    required SidebarXController controller,
  }) : _controller = controller;

  final SidebarXController _controller;

  @override
  Widget build(BuildContext context) {
    return SidebarX(
      controller: _controller,
      theme: SidebarXTheme(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(20),
        ),
        textStyle: TextStyle(color: Colors.black.withOpacity(0.7)),
        selectedTextStyle: const TextStyle(color: Colors.white),
        hoverTextStyle: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w500,
        ),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
          border: Border.all(
            color: Colors.deepPurple.withOpacity(0.37),
          ),
          color: Colors.deepPurple,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.28),
              blurRadius: 30,
            )
          ],
        ),
        iconTheme: const IconThemeData(size: 30),
        selectedIconTheme: const IconThemeData(
          color: Colors.white,
          size: 30,
        ),
      ),
      extendedTheme: const SidebarXTheme(
        width: 300,
      ),
      headerBuilder: (context, extended) {
        final img = Image.asset('assets/${extended ? 'Logo' : 'Logo_B'}.png');
        return Container(
          height: 100,
          color: Colors.white,
          child: SizedBox(child: img),
        );
      },
      items: [
        SidebarXItem(
          icon: Icons.home_outlined,
          label: 'Inicio',
          onTap: () {
            debugPrint('Home');
          },
        ),
        SidebarXItem(
          icon: Icons.shopping_bag_outlined,
          label: 'Ventas',
          onTap: () {
            debugPrint('Ventas');
          },
        ),
        SidebarXItem(
          icon: Icons.sync_alt_sharp,
          label: 'Devoluciones',
          onTap: () {
            debugPrint('Devoluciones');
          },
        ),
        SidebarXItem(
          icon: Icons.file_copy_outlined,
          label: 'Cierre de operaciones',
          onTap: () {
            debugPrint('Cierre de operaciones');
          },
        ),
        SidebarXItem(
          icon: Icons.wallet_outlined,
          label: 'Consulta de saldos y pagos',
          onTap: () {
            debugPrint('Consulta');
          },
        ),
        SidebarXItem(
          icon: Icons.account_balance,
          label: 'Puntos bancarios',
          onTap: () {
            debugPrint('Puntos bancarios');
          },
        ),
        SidebarXItem(
          icon: Icons.discount_outlined,
          label: 'Cupones',
          onTap: () {
            debugPrint('Cupones');
          },
        ),
        SidebarXItem(
          icon: Icons.build_outlined,
          label: 'Reparaciones',
          onTap: () {
            debugPrint('Reparaciones');
          },
        ),
      ],
    );
  }
}
