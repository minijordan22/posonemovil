import 'package:flutter/material.dart';

class ShapeButton extends StatelessWidget {
  final String labelText;
  final String type;
  final double horizontalPadding;
  final IconData? suffixIcon;
  final bool underlineLabelText;
  final IconData? prefixIcon;
  final double verticalPadding;
  final Function()? onPressed;
  final double? minWidth;

  ShapeButton({super.key, 
    required this.labelText, 
    this.type = 'basic', 
    this.horizontalPadding = 0, 
    this.suffixIcon, 
    this.underlineLabelText = false, 
    this.prefixIcon, 
    this.verticalPadding = 0, 
    this.onPressed,
    this.minWidth
  });

  @override
  Widget build(BuildContext context) {
    Color? buttonColor;
    Color textColor;

    switch (type) {
      case 'primary':
        buttonColor = Colors.deepPurple;
        textColor = Colors.white;
        break;
      case 'accent':
        buttonColor = null;
        textColor = Colors.deepPurple;
        break;
      case 'warning':
        buttonColor = Colors.red;
        textColor = Colors.white;
        break;
      case 'basic':
        buttonColor = Colors.white;
        textColor = Colors.deepPurple;
        break;
      default:
        buttonColor = Colors.white;
        textColor = Colors.deepPurple;
    }
    
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      disabledColor: Colors.grey,
      elevation: 0,
      color: buttonColor,
      onPressed: onPressed,
      minWidth: minWidth,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: horizontalPadding, vertical: verticalPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            prefixIcon != null
                ? Icon(
                    prefixIcon,
                    color: textColor,
                  )
                : const SizedBox(
                    width: 10,
                  ),
            Text(
              labelText,
              style: TextStyle(
                  color: textColor,
                  decoration:
                      underlineLabelText ? TextDecoration.underline : null),
            ),
            suffixIcon != null
                ? Icon(
                    suffixIcon,
                    color: textColor,
                  )
                : const SizedBox(
                    width: 10,
                  ),
          ],
        ),
      ),
    );
  }
}