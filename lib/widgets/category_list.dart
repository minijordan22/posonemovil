import 'package:flutter/material.dart';

class CategoryList extends StatelessWidget {
  CategoryList({super.key});
  final itemsList = ['Anillos de compromiso', 'Esclavas', 'Caballero', 'Dama', 'Relojes', 'Argollas de matrimonio', 'Unisex', 'Joyeria fina'];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: 40,
        child: Expanded(
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return _CategoryItem(label: itemsList[index]);
            },
            itemCount: itemsList.length,
          ),
        ));
  }
}

class _CategoryItem extends StatefulWidget {
  final String label;

  _CategoryItem({super.key, required this.label});

  @override
  State<_CategoryItem> createState() => _CategoryItemState();
}

class _CategoryItemState extends State<_CategoryItem> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isSelected = !isSelected;
        });
      },
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Container(
            decoration: BoxDecoration(
              color: isSelected ? Colors.black : Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Center(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(widget.label, style: TextStyle(color: isSelected ? Colors.white : Colors.black)),
            )),
          )),
    );
  }
}
